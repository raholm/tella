cmake_minimum_required(VERSION 3.10.0)

project(tella CXX)

set(${PROJECT}_VERSION_MAJOR 1)
set(${PROJECT}_VERSION_MINOR 0)

# CMake variables
set(CMAKE_BINARY_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/build)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIRECTORY}/bin)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIRECTORY}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIRECTORY}/lib)

# Compiler flags
set(TELLA_COMPILER_FLAGS
  -std=c++17
  -Wall
  -Wpedantic
  -Wextra
  -O2
  -g
  )

set(TELLA_LINKER_FLAGS
  SDL2
  GL
  dl
  )

include_directories(${CMAKE_CURRENT_SOURCE_DIR})
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/thirdparty)
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/thirdparty/glm)

set(GLAD_SOURCES ${CMAKE_CURRENT_SOURCE_DIR}/thirdparty/glad/glad.cpp)

add_subdirectory(tella)

# Target
set(TARGET
  tella
  )

# Root directory
set(ROOT_DIR
  ${CMAKE_CURRENT_SOURCE_DIR}
  )

# Sources
file(GLOB_RECURSE
  SOURCES
  ${ROOT_DIR}/main.cpp
  )

# Compilation
add_executable(${TARGET}
  ${SOURCES}
  ${GLAD_SOURCES}
  )

# Compiler options
target_compile_options(${TARGET}
  PRIVATE
  ${TELLA_COMPILER_FLAGS}
  )

# Library options
target_link_libraries(${TARGET}
  PRIVATE
  ${TELLA_LINKER_FLAGS}
  tella_lib
  )
