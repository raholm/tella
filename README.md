# tella

This is a 3D version of Snake for single player use. You control the snake with W = Up, S = Down, A = Left, D = Right, R = In, F = Out.
You control the view with the arrow keys Left, Right, Up, Down to rotate the camera 90 degrees in the respective direction. The game restarts when you die and to get some
help with dimensionality you can turn on/off help lines by pressing H. Press Escape to quit the game.

![Gameplay](resources/gameplay.gif)
