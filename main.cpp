#include <vector>
#include <random>
#include <array>
#include <chrono>
#include <list>
#include <algorithm>

#include <tella/log.h>
#include <tella/types.h>
#include <tella/window.h>
#include <tella/camera.h>
#include <tella/renderer.h>

int main(int, char**) {
  TELLA_LOG_INFO("Welcome to tella!");

  tella::Window window;
  tella::InputManager input_manager;
  window.register_input_manager(input_manager);

  tella::Snake_Configuration configuration;
  configuration.size = 12;
  configuration.cube_size = 6.0f;

  std::random_device random_device;
  tella::Snake snake = tella::create_snake(configuration, random_device);

  tella::Camera camera = tella::create_camera({ 0.0f, 0.0f, -150.0f },
                                              { 0.0f, 0.0f, -1.0f },
                                              { 0.0f, 1.0f, 0.0f },
                                              static_cast<tella::F32>(window.get_width()) / static_cast<tella::F32>(window.get_height()));

  tella::Renderer renderer;

  {
    const bool success = init(renderer);
    if (!success) return 1;
  }

  std::chrono::steady_clock::time_point previous_time = std::chrono::steady_clock::now();
  std::chrono::steady_clock::time_point current_time;

  while (!window.should_close()) {
    current_time = std::chrono::steady_clock::now();

    const tella::F32 timestep = std::chrono::duration_cast<std::chrono::milliseconds>(current_time - previous_time).count() / 1000.0f;

    previous_time = current_time;

    window.poll_events();

    const bool dead = step(snake, camera, timestep, input_manager);
    render(renderer, camera, snake);

    window.swap_buffer();

    if (dead)
      snake = create_snake(configuration, random_device);
  }

  deinit(renderer);
  return 0;
}
