#!/bin/bash

SCRIPT_FOLDER=`dirname $0`
BINARY_FOLDER=${SCRIPT_FOLDER}/build/bin
EXECUTABLE=${BINARY_FOLDER}/tella

if [ ! -f ${EXECUTABLE} ]; then
    echo "Please build tella!"
    exit 1
fi

${EXECUTABLE}
