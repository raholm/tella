#!/bin/bash

SCRIPT_FOLDER=`dirname $0`
BUILD_FOLDER=${SCRIPT_FOLDER}/build
BUILD_TARGET=$1

if [ -z ${BUILD_TARGET} ]; then
    BUILD_TARGET="all"
fi

if [ ! -d ${BUILD_FOLDER} ]; then
    echo "Creating ${BUILD_FOLDER}..."
    mkdir -p ${BUILD_FOLDER}
fi

cd ${BUILD_FOLDER}
cmake ..
cmake --build . --parallel 12 --target ${BUILD_TARGET}
