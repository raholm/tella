#include "input_manager.h"

namespace tella {

  void InputManager::set_pressed(const KeyboardKey key)
  {
    m_pressed_keys.set(to_integral(key));
  }

  void InputManager::set_held(const KeyboardKey key)
  {
    set_pressed(key);
    m_held_keys.set(to_integral(key));
  }

  void InputManager::set_released(const KeyboardKey key)
  {
    m_released_keys.set(to_integral(key));
  }

  void InputManager::reset_pressed(const KeyboardKey key)
  {
    m_pressed_keys.reset(to_integral(key));
    m_held_keys.reset(to_integral(key));
  }

  void InputManager::reset_released()
  {
    m_released_keys.reset();
  }

  bool InputManager::is_pressed(const KeyboardKey key) const
  {
    return m_pressed_keys.test(to_integral(key));
  }

  bool InputManager::is_held(const KeyboardKey key) const
  {
    return m_held_keys.test(to_integral(key));
  }

  bool InputManager::is_released(const KeyboardKey key) const
  {
    return m_released_keys.test(to_integral(key));
  }

}  // tella
