
#include "renderer.h"
#include "log.h"

namespace tella {

#define GL_ASSERT                                             \
  do {                                                        \
    auto gl_error = glGetError();                             \
    if (gl_error != GL_NO_ERROR) {                            \
      TELLA_LOG_ERROR("error: " << gl_error); fflush(stdout); \
      assert(false);                                          \
    }                                                         \
  } while (false)

  constexpr const char* environment_vertex_shader = R"(
#version 330 core
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec3 in_position;

out vec4 vertex_color;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

void main()
{
  gl_Position = projection * view * model * vec4(in_position, 1.0);
  vertex_color = vec4(0.0, 0.0, 0.0, 1.0);
}
)";

  constexpr const char* environment_fragment_shader = R"(
#version 330 core
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec4 vertex_color;

out vec4 out_color;

void main()
{
  out_color = vertex_color;
}
)";

  constexpr const char* object_vertex_shader = R"(
#version 330 core
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec3 in_position;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

void main()
{
  gl_Position = projection * view * model * vec4(in_position, 1.0);
}
)";

  constexpr const char* object_fragment_shader = R"(
#version 330 core
#extension GL_ARB_separate_shader_objects : enable

uniform vec4 color;

out vec4 out_color;

void main()
{
  out_color = color;
}
)";

  constexpr const std::array<float, 12> quad_vertices = {
    0.5f,  0.5f, 0.0f,  // top right
    0.5f, -0.5f, 0.0f,  // bottom right
    -0.5f, -0.5f, 0.0f,  // bottom left
    -0.5f,  0.5f, 0.0f   // top left
  };

  const std::array<U32, 6> quad_indices = {
    0, 1, 3,   // first triangle
    1, 2, 3    // second triangle
  };


  const std::array<float, 24> cube_vertices = {
    -0.5f, -0.5f,  0.5f, // 2 <- 0
    0.5f, -0.5f,  0.5f, // 4 <- 1
    -0.5f,  0.5f,  0.5f, // 1 <- 2
    0.5f,  0.5f,  0.5f, // 3 <- 3
    -0.5f, -0.5f, -0.5f, // 6 <- 4
    0.5f, -0.5f, -0.5f, // 8 <- 5
    -0.5f,  0.5f, -0.5f, // 5 <- 6
    0.5f,  0.5f, -0.5f  // 7 <- 7
  };

  const std::array<U32, 36> cube_indices = {
    // Top
    7, 6, 2,
    2, 3, 7,

    // Bottom
    0, 4, 5,
    5, 1, 0,

    // Left
    0, 2, 6,
    6, 4, 0,

    // Right
    7, 3, 1,
    1, 5, 7,

    // Front
    3, 2, 0,
    0, 1, 3,

    // Back
    4, 6, 7,
    7, 5, 4,
  };

  const std::array<U32, 24> cube_line_indices = {
    // Front
    2, 3,
    0, 1,
    0, 2,
    1, 3,

    // Back
    6, 7,
    4, 5,
    6, 4,
    7, 5,

    // Left
    2, 6,
    0, 4,

    // Right
    3, 7,
    1, 5,
  };

  static bool init(Environment_Renderer& renderer)
  {
    TELLA_LOG_DEBUG("Initialize environment renderer...");

    GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);

    S32 success;
    std::array<GLchar, 512> error_log;

    glShaderSource(vertex_shader, 1, &environment_vertex_shader, nullptr);
    glCompileShader(vertex_shader);

    glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &success);

    if (!success) {
      error_log.fill(0);
      glGetShaderInfoLog(vertex_shader, error_log.size(), nullptr, error_log.data());
      TELLA_LOG_ERROR("Failed to compile vertex shader: " << std::string(error_log.begin(), error_log.end()));
      return false;
    }

    glShaderSource(fragment_shader, 1, &environment_fragment_shader, nullptr);
    glCompileShader(fragment_shader);

    glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &success);

    if (!success) {
      error_log.fill(0);
      glGetShaderInfoLog(fragment_shader, error_log.size(), nullptr, error_log.data());
      TELLA_LOG_ERROR("Failed to compile fragment shader: " << std::string(error_log.begin(), error_log.end()));
      return false;
    }

    TELLA_LOG_DEBUG("Creating environment shader program...");

    renderer.shader_program = glCreateProgram();
    glAttachShader(renderer.shader_program, vertex_shader);
    glAttachShader(renderer.shader_program, fragment_shader);
    glLinkProgram(renderer.shader_program);

    glGetProgramiv(renderer.shader_program, GL_LINK_STATUS, &success);

    if (!success) {
      error_log.fill(0);
      glGetProgramInfoLog(fragment_shader, error_log.size(), nullptr, error_log.data());
      TELLA_LOG_ERROR("Failed to link shader program: " << std::string(error_log.begin(), error_log.end()));
    }

    glDeleteShader(vertex_shader);
    glDeleteShader(fragment_shader);

    renderer.projection_uniform_location = glGetUniformLocation(renderer.shader_program, "projection");
    renderer.view_uniform_location = glGetUniformLocation(renderer.shader_program, "view");
    renderer.model_uniform_location = glGetUniformLocation(renderer.shader_program, "model");

    // Generate objects
    glGenVertexArrays(1, &renderer.vertex_array_object);
    glGenBuffers(1, &renderer.vertex_buffer_object);
    glGenBuffers(1, &renderer.element_array_object);

    // VAO
    glBindVertexArray(renderer.vertex_array_object);

    // VBO
    glBindBuffer(GL_ARRAY_BUFFER, renderer.vertex_buffer_object);

    glBufferData(GL_ARRAY_BUFFER,
                 cube_vertices.size() * sizeof(cube_vertices[0]),
                 cube_vertices.data(),
                 GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(cube_vertices[0]), (void*) 0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // EAO
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, renderer.element_array_object);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                 cube_line_indices.size() * sizeof(cube_line_indices[0]),
                 cube_line_indices.data(),
                 GL_STATIC_DRAW);

    glBindVertexArray(0);
    return true;
  }

  static void deinit(Environment_Renderer& renderer)
  {
    TELLA_LOG_DEBUG("Deinitialize environment renderer...");

    glDeleteVertexArrays(1, &renderer.vertex_array_object);
    glDeleteBuffers(1, &renderer.vertex_buffer_object);
    glDeleteBuffers(1, &renderer.element_array_object);
    glDeleteProgram(renderer.shader_program);
  }

  static void render(Environment_Renderer& renderer,
                     const glm::mat4& projection_matrix,
                     const glm::mat4& view_matrix,
                     const Snake& snake)
  {
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    glUseProgram(renderer.shader_program);
    glBindVertexArray(renderer.vertex_array_object);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, renderer.element_array_object);

    glUniformMatrix4fv(renderer.projection_uniform_location, 1, GL_FALSE, glm::value_ptr(projection_matrix));
    glUniformMatrix4fv(renderer.view_uniform_location, 1, GL_FALSE, glm::value_ptr(view_matrix));

    const auto snake_head_position = snake.snake_position_list.front();
    const auto apple_position = snake.apple_position;

    glLineWidth(1.0f);

    if (snake.render_help_lines)
    {
      for (const auto& transform : snake.environment_model_transform_list)
      {
        const auto position = std::get<0>(transform);

        if (!(position.row == snake_head_position.row ||
              position.row == apple_position.row ||
              position.col == snake_head_position.col ||
              position.col == apple_position.col ||
              position.depth == snake_head_position.depth ||
              position.depth == apple_position.depth))
          continue;

        glUniformMatrix4fv(renderer.model_uniform_location, 1, GL_FALSE, glm::value_ptr(std::get<1>(transform)));
        glDrawElements(GL_LINES, cube_line_indices.size(), GL_UNSIGNED_INT, 0);
      }
    }

    glLineWidth(4.0f);

    // Draw the whole box
    const auto total_size = snake.configuration.size * snake.configuration.cube_size;
    glm::mat4 transform = glm::mat4(1.0f);
    transform = glm::scale(transform, glm::vec3(total_size, total_size, total_size));
    glUniformMatrix4fv(renderer.model_uniform_location, 1, GL_FALSE, glm::value_ptr(transform));
    glDrawElements(GL_LINES, cube_line_indices.size(), GL_UNSIGNED_INT, 0);

    // Render lines onto snake/apple for cartoonish feeling
    {
      // Render Snake
      for (const auto& position : snake.snake_position_list)
      {
        glUniformMatrix4fv(renderer.model_uniform_location, 1, GL_FALSE,
                           glm::value_ptr(get_transform_matrix(snake,
                                                               position.row,
                                                               position.col,
                                                               position.depth)));
        glDrawElements(GL_LINES, cube_line_indices.size(), GL_UNSIGNED_INT, 0);
      }

      // Render Apple
      glUniformMatrix4fv(renderer.model_uniform_location, 1, GL_FALSE,
                         glm::value_ptr(get_transform_matrix(snake,
                                                             snake.apple_position.row,
                                                             snake.apple_position.col,
                                                             snake.apple_position.depth)));
      glDrawElements(GL_LINES, cube_line_indices.size(), GL_UNSIGNED_INT, 0);
    }
  }

  static bool init(Entity_Renderer& renderer)
  {
    TELLA_LOG_DEBUG("Initialize entity renderer...");

    GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);

    S32 success;
    std::array<GLchar, 512> error_log;

    glShaderSource(vertex_shader, 1, &object_vertex_shader, nullptr);
    glCompileShader(vertex_shader);

    glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &success);

    if (!success) {
      error_log.fill(0);
      glGetShaderInfoLog(vertex_shader, error_log.size(), nullptr, error_log.data());
      TELLA_LOG_ERROR("Failed to compile vertex shader: " << std::string(error_log.begin(), error_log.end()));
    }

    glShaderSource(fragment_shader, 1, &object_fragment_shader, nullptr);
    glCompileShader(fragment_shader);

    glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &success);

    if (!success) {
      error_log.fill(0);
      glGetShaderInfoLog(fragment_shader, error_log.size(), nullptr, error_log.data());
      TELLA_LOG_ERROR("Failed to compile fragment shader: " << std::string(error_log.begin(), error_log.end()));
      return false;
    }

    TELLA_LOG_DEBUG("Creating object shader program...");

    renderer.shader_program = glCreateProgram();

    // @note: Had some misfortune with gl hence these asserts
    GL_ASSERT;
    glAttachShader(renderer.shader_program, vertex_shader);

    GL_ASSERT;
    glAttachShader(renderer.shader_program, fragment_shader);

    GL_ASSERT;
    glLinkProgram(renderer.shader_program);

    GL_ASSERT;
    glGetProgramiv(renderer.shader_program, GL_LINK_STATUS, &success);

    if (!success) {
      error_log.fill(0);
      GL_ASSERT;
      glGetProgramInfoLog(fragment_shader, error_log.size(), nullptr, error_log.data());
      TELLA_LOG_ERROR("Failed to link shader program: " << std::string(error_log.begin(), error_log.end()));
      return false;
    }

    GL_ASSERT;
    glDeleteShader(vertex_shader);

    GL_ASSERT;
    glDeleteShader(fragment_shader);

    renderer.projection_uniform_location = glGetUniformLocation(renderer.shader_program, "projection");
    renderer.view_uniform_location = glGetUniformLocation(renderer.shader_program, "view");
    renderer.model_uniform_location = glGetUniformLocation(renderer.shader_program, "model");
    renderer.color_uniform_location = glGetUniformLocation(renderer.shader_program, "color");

    // Generate objects
    GL_ASSERT;
    glGenVertexArrays(1, &renderer.vertex_array_object);

    GL_ASSERT;
    glGenBuffers(1, &renderer.vertex_buffer_object);

    GL_ASSERT;
    glGenBuffers(1, &renderer.element_array_object);

    // VAO
    GL_ASSERT;
    glBindVertexArray(renderer.vertex_array_object);

    // VBO
    GL_ASSERT;
    glBindBuffer(GL_ARRAY_BUFFER, renderer.vertex_buffer_object);

    GL_ASSERT;
    glBufferData(GL_ARRAY_BUFFER,
                 cube_vertices.size() * sizeof(cube_vertices[0]),
                 cube_vertices.data(),
                 GL_STATIC_DRAW);

    GL_ASSERT;
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(cube_vertices[0]), (void*) 0);

    GL_ASSERT;
    glEnableVertexAttribArray(0);

    GL_ASSERT;
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // EAO
    GL_ASSERT;
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, renderer.element_array_object);

    GL_ASSERT;
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                 cube_indices.size() * sizeof(cube_indices[0]),
                 cube_indices.data(),
                 GL_STATIC_DRAW);

    glBindVertexArray(0);
    return true;
  }

  static void deinit(Entity_Renderer& renderer)
  {
    TELLA_LOG_DEBUG("Deinitialize entity renderer...");

    glDeleteVertexArrays(1, &renderer.vertex_array_object);
    glDeleteBuffers(1, &renderer.vertex_buffer_object);
    glDeleteBuffers(1, &renderer.element_array_object);
    glDeleteProgram(renderer.shader_program);
  }

  static void render(Entity_Renderer& renderer,
                     const glm::mat4& projection_matrix,
                     const glm::mat4& view_matrix,
                     const Snake& snake)
  {
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    glUseProgram(renderer.shader_program);
    glBindVertexArray(renderer.vertex_array_object);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, renderer.element_array_object);

    glUniformMatrix4fv(renderer.projection_uniform_location, 1, GL_FALSE, glm::value_ptr(projection_matrix));
    glUniformMatrix4fv(renderer.view_uniform_location, 1, GL_FALSE, glm::value_ptr(view_matrix));

    // Render Snake
    for (const auto& position : snake.snake_position_list)
    {
      glUniformMatrix4fv(renderer.model_uniform_location, 1, GL_FALSE,
                         glm::value_ptr(get_transform_matrix(snake,
                                                             position.row,
                                                             position.col,
                                                             position.depth)));
      glUniform4f(renderer.color_uniform_location, 60.0f / 255.0f, 150.0f / 255.0f, 105.0f / 255.0f, 1.0f);
      glDrawElements(GL_TRIANGLES, cube_indices.size(), GL_UNSIGNED_INT, 0);
    }

    // Render Apple
    glUniformMatrix4fv(renderer.model_uniform_location, 1, GL_FALSE,
                       glm::value_ptr(get_transform_matrix(snake,
                                                           snake.apple_position.row,
                                                           snake.apple_position.col,
                                                           snake.apple_position.depth)));
    glUniform4f(renderer.color_uniform_location, 150.0f / 255.0f, 0.0f, 0.0f, 1.0f);
    glDrawElements(GL_TRIANGLES, cube_indices.size(), GL_UNSIGNED_INT, 0);
  }


  bool init(Renderer& renderer)
  {
    TELLA_LOG_DEBUG("Initialize renderer...");

    if (!init(renderer.environment))
    {
      return false;
    }

    if (!init(renderer.entity))
    {
      return false;
    }

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glFrontFace(GL_CCW);
    return true;
  }

  void deinit(Renderer& renderer)
  {
    TELLA_LOG_DEBUG("Deinitialize renderer...");

    deinit(renderer.environment);
    deinit(renderer.entity);
  }

  void render(Renderer& renderer, const Camera& camera, const Snake& snake)
  {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.811f, 0.490f, 0.478f, 1.0f);

    const auto projection_matrix = get_projection_matrix(camera);
    const auto view_matrix = get_view_matrix(camera);

    render(renderer.environment, projection_matrix, view_matrix, snake);
    render(renderer.entity, projection_matrix, view_matrix, snake);
  }

}  // tella
