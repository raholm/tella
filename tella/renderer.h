#pragma once

#include <glad/glad.h>
#include <GL/gl.h>
#include <GL/glx.h>

#include "camera.h"
#include "snake.h"

namespace tella {

  struct Environment_Renderer
  {
    GLuint shader_program;

    GLuint vertex_buffer_object;
    GLuint vertex_array_object;
    GLuint element_array_object;

    GLint projection_uniform_location;
    GLint view_uniform_location;
    GLint model_uniform_location;
  };

  struct Entity_Renderer
  {
    GLuint shader_program;

    GLuint vertex_buffer_object;
    GLuint vertex_array_object;
    GLuint element_array_object;

    GLint projection_uniform_location;
    GLint view_uniform_location;
    GLint model_uniform_location;
    GLint color_uniform_location;
  };

  struct Renderer
  {
    Environment_Renderer environment;
    Entity_Renderer entity;
  };

  bool init(Renderer& renderer);
  void deinit(Renderer& renderer);

  void render(Renderer& renderer, const Camera& camera, const Snake& snake);

}  // tella
