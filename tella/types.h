#pragma once

#include <stdint.h>
#include <cstdlib>

namespace tella {

  using UChar = unsigned char;
  using Byte =  char;
  using UByte = UChar;
  using S8 = int8_t;
  using U8 = uint8_t;
  using S16 = int16_t;
  using U16 = uint16_t;
  using S32 = int32_t;
  using U32 = uint32_t;
  using S64 = int64_t;
  using U64 = uint64_t;
  using F32 = float;
  using F64 = double;
  using Size = std::size_t;

}  // tella
