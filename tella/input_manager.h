#pragma once

#include <type_traits>
#include <bitset>

namespace tella {

  template<typename Enum>
  constexpr auto to_integral(const Enum value) -> typename std::underlying_type<Enum>::type
  {
    return static_cast<typename std::underlying_type<Enum>::type>(value);
  }

  enum class KeyboardKey
  {
    W = 0,
    S,
    A,
    D,
    R,
    F,
    H,

    Up,
    Down,
    Left,
    Right,

    CtrlLeft,
    CtrlRight,

    AltLeft,
    AltRight,

    Escape,

    Unknown,
    Count,
  };

  class InputManager final
  {
  public:
    InputManager() = default;

    ~InputManager() = default;

    void set_pressed(const KeyboardKey key);
    void set_held(const KeyboardKey key);
    void set_released(const KeyboardKey key);

    void reset_pressed(const KeyboardKey key);
    void reset_released();

    bool is_pressed(const KeyboardKey key) const;
    bool is_held(const KeyboardKey key) const;
    bool is_released(const KeyboardKey key) const;

  private:
    InputManager(const InputManager&) = delete;
    InputManager(InputManager&&) = delete;

    InputManager& operator=(const InputManager&) = delete;
    InputManager& operator=(InputManager&&) = delete;

  private:
    std::bitset<to_integral(KeyboardKey::Count)> m_pressed_keys;
    std::bitset<to_integral(KeyboardKey::Count)> m_held_keys;
    std::bitset<to_integral(KeyboardKey::Count)> m_released_keys;

  };

}  // tella
