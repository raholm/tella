#pragma once

#include <random>
#include <list>

#include <glm/mat4x4.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>

#include "types.h"
#include "camera.h"
#include "input_manager.h"

namespace tella {

  struct Snake_Position
  {
    constexpr bool operator==(const Snake_Position rhs) const {
      return row == rhs.row && col == rhs.col && depth == rhs.depth;
    }

    U32 row;
    U32 col;
    U32 depth;
  };

  struct Snake_Configuration
  {
    U32 size;
    F32 cube_size;
  };

  struct Snake
  {
    Snake_Configuration configuration;

    std::mt19937 generator;
    std::uniform_int_distribution<U32> distribution;

    glm::vec3 direction;
    F32 update_rate;
    F32 time_since_last_update;
    std::list<Snake_Position> snake_position_list;
    Snake_Position apple_position;

    std::vector<std::tuple<Snake_Position, glm::mat4>> environment_model_transform_list;

    bool render_help_lines;
  };

  Snake create_snake(const Snake_Configuration configuration, std::random_device& random_device);
  glm::mat4 get_transform_matrix(const Snake& snake, const U32 row, const U32 col, const U32 depth);
  bool step(Snake& snake, Camera& camera, const F32 timestep, const InputManager& input_manager);

}  // tella
