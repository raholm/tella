#pragma once

#include <glm/mat4x4.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/ext/matrix_clip_space.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "types.h"

namespace tella {

  struct Camera
  {
    glm::vec3 position;
    glm::vec3 target;
    glm::vec3 front;
    glm::vec3 up;
    glm::vec3 right;

    F32 field_of_view { 0.0f };
    F32 aspect_ratio { 0.0f };
    F32 near { 0.0 };
    F32 far { 0.0 };
  };

  Camera create_camera(const glm::vec3& position,
                       const glm::vec3& target,
                       const glm::vec3& up,
                       const F32 aspect_ratio);
  void recompute_coordinate_system(Camera& camera,
                                   const glm::vec3 position,
                                   const glm::vec3 up);

  glm::mat4 get_projection_matrix(const Camera& camera);
  glm::mat4 get_view_matrix(const Camera& camera);

}  // tella
