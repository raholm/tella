#pragma once

#include <iostream>
#include <iomanip>

#include "types.h"

namespace tella {

  enum class LogLevel
  {
    Internal,
    Debug,
    Info,
    Warning,
    Error,
    Critical,
  };

  void log(const LogLevel log_level,
           const std::string& message,
           const std::string& file_path,
           const U32 line_number);

} // tella

#define TELLA_FORMAT_MESSAGE(items) ((dynamic_cast<std::ostringstream&>(std::ostringstream().seekp(0, std::ios_base::cur) << items)).str())
#define TELLA_LOG_INTERNAL(message) tella::log(tella::LogLevel::Internal, TELLA_FORMAT_MESSAGE(message), __FILE__, __LINE__)
#define TELLA_LOG_DEBUG(message) tella::log(tella::LogLevel::Debug, TELLA_FORMAT_MESSAGE(message), __FILE__, __LINE__)
#define TELLA_LOG_INFO(message) tella::log(tella::LogLevel::Info, TELLA_FORMAT_MESSAGE(message), __FILE__, __LINE__)
#define TELLA_LOG_WARNING(message) tella::log(tella::LogLevel::Warning, TELLA_FORMAT_MESSAGE(message), __FILE__, __LINE__)
#define TELLA_LOG_ERROR(message) tella::log(tella::LogLevel::Error, TELLA_FORMAT_MESSAGE(message), __FILE__, __LINE__)
#define TELLA_LOG_CRITICAL(message) tella::log(tella::LogLevel::Critical, TELLA_FORMAT_MESSAGE(message), __FILE__, __LINE__)
