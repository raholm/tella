#include "camera.h"

namespace tella {

  Camera create_camera(const glm::vec3& position,
                       const glm::vec3& target,
                       const glm::vec3& up,
                       const F32 aspect_ratio) {
    Camera camera;
    camera.position = position;
    camera.target = target;
    camera.front = glm::normalize(position - target);
    camera.right = glm::normalize(glm::cross(up, camera.front));
    camera.up = glm::cross(camera.front, camera.right);
    camera.field_of_view = glm::radians(45.0f);
    camera.aspect_ratio = aspect_ratio;
    camera.near = 0.001f;
    camera.far = 10000.0f;

    recompute_coordinate_system(camera, position, up);
    return camera;
  }

  void recompute_coordinate_system(Camera& camera,
                                   const glm::vec3 position,
                                   const glm::vec3 up)
  {
    camera.position = position;
    camera.up = up;
    camera.front = glm::normalize(camera.position - camera.target);
    camera.right = glm::normalize(glm::cross(up, camera.front));

    // @note: We do some stability here to avoid wobbling when changing view because of rounding stuff
    if (std::abs(camera.position.x) < 10) {
      camera.position.x = 0.0f;
    }
    else {
      if (camera.position.x > 0.0f) {
        camera.position.x = 150.0f;
      }
      else {
        camera.position.x = -150.0f;
      }
    }

    if (std::abs(camera.position.y) < 10) {
      camera.position.y = 0.0f;
    }
    else {
      if (camera.position.y > 0.0f) {
        camera.position.y = 150.0f;
      }
      else {
        camera.position.y = -150.0f;
      }
    }

    if (std::abs(camera.position.z) < 10) {
      camera.position.z = 0.0f;
    }
    else {
      if (camera.position.z > 0.0f) {
        camera.position.z = 150.0f;
      }
      else {
        camera.position.z = -150.0f;
      }
    }

    camera.up.x = std::round(camera.up.x);
    camera.up.y = std::round(camera.up.y);
    camera.up.z = std::round(camera.up.z);

    camera.front.x = std::round(camera.front.x);
    camera.front.y = std::round(camera.front.y);
    camera.front.z = std::round(camera.front.z);

    camera.right.x = std::round(camera.right.x);
    camera.right.y = std::round(camera.right.y);
    camera.right.z = std::round(camera.right.z);
  }

  glm::mat4 get_projection_matrix(const Camera& camera)
  {
    return glm::perspective(camera.field_of_view,
                            camera.aspect_ratio,
                            camera.near,
                            camera.far);
  }

  glm::mat4 get_view_matrix(const Camera& camera)
  {
    return glm::lookAt(camera.position, camera.position - camera.front, camera.up);
  }

}  // tella
