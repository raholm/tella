#include <algorithm>

#include "snake.h"
#include "log.h"

namespace tella {

  static Snake_Position generate_apple_position(Snake& snake)
  {
    Snake_Position result;

    do {
      result.row = snake.distribution(snake.generator);
      result.col = snake.distribution(snake.generator);
      result.depth = snake.distribution(snake.generator);
    } while(std::find(snake.snake_position_list.cbegin(),
                      snake.snake_position_list.cend(),
                      result) != snake.snake_position_list.cend());

    return result;
  }

  Snake create_snake(const Snake_Configuration configuration, std::random_device& random_device)
  {
    Snake snake;
    snake.configuration = configuration;
    snake.generator = std::mt19937(random_device());
    snake.distribution = std::uniform_int_distribution<U32>(0, configuration.size - 1);

    snake.direction = { 0.0f, 1.0f, 0.0f };
    snake.update_rate = 0.1f;
    snake.time_since_last_update = 0.0f;

    snake.snake_position_list.push_back({ 2, 0, 0 });
    snake.snake_position_list.push_back({ 1, 0, 0 });
    snake.snake_position_list.push_back({ 0, 0, 0 });
    snake.apple_position = generate_apple_position(snake);

    snake.render_help_lines = false;

    for (U32 row = 0; row < configuration.size; ++row)
      for (U32 col = 0; col < configuration.size; ++col)
        for (U32 depth = 0; depth < configuration.size; ++depth)
          snake.environment_model_transform_list.emplace_back(std::make_tuple(Snake_Position { row, col, depth}, get_transform_matrix(snake, row, col, depth)));

    return snake;
  }

  glm::mat4 get_transform_matrix(const Snake& snake, const U32 row, const U32 col, const U32 depth)
  {
    const auto total_size = snake.configuration.size * snake.configuration.cube_size;
    const auto half_total_size = total_size / 2.0f;
    const auto cube_size = snake.configuration.cube_size;
    const auto half_cube_size = cube_size / 2.0f;

    const auto position = glm::vec3(half_cube_size + col * cube_size,
                                    half_cube_size + row * cube_size,
                                    half_cube_size + depth * cube_size) - half_total_size;

    glm::mat4 transform = glm::mat4(1.0f);
    transform = glm::translate(transform, position);
    transform = glm::scale(transform, glm::vec3(cube_size, cube_size, cube_size));
    return transform;
  }

  bool step(Snake& snake, Camera& camera, const F32 timestep, const InputManager& input_manager)
  {
    bool changed_view = false;

    glm::mat4 transform = glm::mat4(1.0f);

    if (input_manager.is_released(tella::KeyboardKey::Up))
    {
      transform = glm::rotate(transform, glm::radians(-90.0f), camera.right);
      changed_view = true;
    }
    else if (input_manager.is_released(tella::KeyboardKey::Down))
    {
      transform = glm::rotate(transform, glm::radians(90.0f), camera.right);
      changed_view = true;
    }
    else if (input_manager.is_released(tella::KeyboardKey::Left))
    {
      transform = glm::rotate(transform, glm::radians(-90.0f), camera.up);
      changed_view = true;
    }
    else if (input_manager.is_released(tella::KeyboardKey::Right))
    {
      transform = glm::rotate(transform, glm::radians(90.0f), camera.up);
      changed_view = true;
    }
    else if (input_manager.is_released(tella::KeyboardKey::H))
    {
      snake.render_help_lines = !snake.render_help_lines;
    }

    if (changed_view)
    {
      recompute_coordinate_system(camera,
                                  transform * glm::vec4(camera.position, 0.0),
                                  transform * glm::vec4(camera.up, 0.0));
    }

    const auto previous_direction = snake.direction;

    if (input_manager.is_pressed(tella::KeyboardKey::W))
      snake.direction = camera.up;
    else if (input_manager.is_pressed(tella::KeyboardKey::S))
      snake.direction = -camera.up;
    else if (input_manager.is_pressed(tella::KeyboardKey::A))
      snake.direction = -camera.right;
    else if (input_manager.is_pressed(tella::KeyboardKey::D))
      snake.direction = camera.right;
    else if (input_manager.is_pressed(tella::KeyboardKey::R))
      snake.direction = -camera.front;
    else if (input_manager.is_pressed(tella::KeyboardKey::F))
      snake.direction = camera.front;

    // @note: We should not be able to change to opposite direction
    if (glm::length(snake.direction + previous_direction) == 0.0f) {
      snake.direction = previous_direction;
    }

    snake.time_since_last_update += timestep;

    if (snake.time_since_last_update >= snake.update_rate) {
      snake.time_since_last_update = 0.0f;

      auto new_position = snake.snake_position_list.front();

      auto col_direction = static_cast<S32>(std::round(snake.direction.x));
      auto row_direction = static_cast<S32>(std::round(snake.direction.y));
      auto depth_direction = static_cast<S32>(std::round(snake.direction.z));


      if (row_direction == -1 && new_position.row == 0)
      {
        new_position.row = snake.configuration.size - 1;
      }
      else
      {
        new_position.row += row_direction;
        new_position.row %= snake.configuration.size;
      }

      if (col_direction == -1 && new_position.col == 0)
      {
        new_position.col = snake.configuration.size - 1;
      }
      else
      {
        new_position.col += col_direction;
        new_position.col %= snake.configuration.size;
      }

      if (depth_direction == -1 && new_position.depth == 0)
      {
        new_position.depth = snake.configuration.size - 1;
      }
      else
      {
        new_position.depth += depth_direction;
        new_position.depth %= snake.configuration.size;
      }

      for (const auto position : snake.snake_position_list)
      {
        if (position == new_position)
        {
          TELLA_LOG_INFO("WE DEAD: " << snake.snake_position_list.size());
          return true;
        }
      }

      snake.snake_position_list.push_front(new_position);

      if (new_position == snake.apple_position) {
        snake.apple_position = generate_apple_position(snake);
      }
      else {
        snake.snake_position_list.pop_back();
      }
    }

    return false;
  }

}  // tella
