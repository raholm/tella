#include <sstream>
#include <chrono>
#include <thread>
#include <mutex>

#include "log.h"

namespace tella {

  namespace {

    std::string to_color_string(const LogLevel log_level)
    {
      switch(log_level)
      {
      case LogLevel::Internal:
        return "\033[0;32mINTERNAL\033[0m";
      case LogLevel::Debug:
        return "\033[0;34mDEBUG\033[0m";
      case LogLevel::Info:
        return "\033[0;36mINFO\033[0m";
      case LogLevel::Warning:
        return "\033[0;33mWARNING\033[0m";
      case LogLevel::Error:
        return "\033[0;31mERROR\033[0m";
      case LogLevel::Critical:
        return "\033[0;35mCRITICAL\033[0m";
      default:
        return "UNKOWN";
      }
    }

    std::string current_datetime_string()
    {
      const auto now = std::chrono::system_clock::now();
      const auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch()) % 1000;
      auto timer = std::chrono::system_clock::to_time_t(now);

      std::ostringstream output;
      output << std::put_time(std::localtime(&timer), "%Y-%m-%d %H:%M:%S") << "."
             << std::setfill('0') << std::setw(3) << milliseconds.count();
      return output.str();
    }

  }

  void log(const LogLevel log_level,
           const std::string& message,
           const std::string& file_path,
           const U32 line_number)
  {
    static std::mutex mutex;

    std::lock_guard<std::mutex> lock_guard(mutex);

    const char* file_name = file_path.c_str() + file_path.length() - 1;

    while (file_name && file_name[0] != '/')
      file_name--;

    if (file_name)
      file_name++;

    std::cerr << "[" << std::left << std::setw(22) << current_datetime_string() << "|"
              << std::right << std::setw(8) << to_color_string(log_level) << "|"
              << "T" << std::this_thread::get_id() << "|"
              << file_name << ":" << line_number << "]$ "
              << message << std::endl;
  }

}  // tella
