#include "window.h"
#include "log.h"

#include <glad/glad.h>

namespace tella {

  namespace {

    KeyboardKey convert(const SDL_Event& event)
    {
      switch (event.key.keysym.sym) {
      case SDLK_ESCAPE: return KeyboardKey::Escape;
      case SDLK_w: return KeyboardKey::W;
      case SDLK_s: return KeyboardKey::S;
      case SDLK_a: return KeyboardKey::A;
      case SDLK_d: return KeyboardKey::D;
      case SDLK_r: return KeyboardKey::R;
      case SDLK_f: return KeyboardKey::F;
      case SDLK_h: return KeyboardKey::H;
      case SDLK_LEFT: return KeyboardKey::Left;
      case SDLK_RIGHT: return KeyboardKey::Right;
      case SDLK_UP: return KeyboardKey::Up;
      case SDLK_DOWN: return KeyboardKey::Down;
      case SDLK_LALT: return KeyboardKey::AltLeft;
      case SDLK_RALT: return KeyboardKey::AltRight;
      case SDLK_LCTRL: return KeyboardKey::CtrlLeft;
      case SDLK_RCTRL: return KeyboardKey::CtrlRight;
      default: return KeyboardKey::Unknown;
      }
    }

  }  //

  Window::Settings::Settings()
    : width(1920),
      height(1080),
      title("tella")
  {

  }

  Window::Window(const Settings& settings)
    : m_settings(settings),
      m_should_close(false),
      m_handle(nullptr),
      m_context(nullptr),
      m_input_manager(nullptr)
  {
    TELLA_LOG_DEBUG("Initializing SDL...");

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
      TELLA_LOG_ERROR("Failed to initialize SDL: " << SDL_GetError());
      return;
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

    const U32 window_flags =
      SDL_WINDOW_SHOWN |
      // SDL_WINDOW_RESIZABLE |
      SDL_WINDOW_OPENGL;

    TELLA_LOG_DEBUG("Creating window...");

    m_handle = SDL_CreateWindow(m_settings.title.c_str(),
                                SDL_WINDOWPOS_CENTERED,
                                SDL_WINDOWPOS_CENTERED,
                                m_settings.width,
                                m_settings.height,
                                window_flags);

    if (!m_handle) {
      TELLA_LOG_ERROR("Failed to create window: " << SDL_GetError());
      SDL_Quit();
      return;
    }

    TELLA_LOG_DEBUG("Creating OpenGL context...");

    m_context = SDL_GL_CreateContext(m_handle);

    if (!m_context) {
      TELLA_LOG_ERROR("Failed to create OpenGL context: " << SDL_GetError());
      destroy();
      return;
    }

    if (SDL_GL_MakeCurrent(m_handle, m_context)) {
      TELLA_LOG_ERROR("Failed to activate OpenGL context: " << SDL_GetError());
      destroy();
      return;
    }

    if (!gladLoadGLLoader((GLADloadproc) SDL_GL_GetProcAddress)) {
      TELLA_LOG_ERROR("Failed to initialize glad");
      destroy();
      return;
    }

    // SDL_SetRelativeMouseMode(SDL_TRUE);
  }

  Window::~Window()
  {
    destroy();
  }

  bool Window::is_null() const
  {
    return m_handle == nullptr;
  }

  bool Window::should_close() const
  {
    return m_should_close;
  }

  void Window::close()
  {
    m_should_close = true;
  }

  void Window::poll_events()
  {
    SDL_Event event;

    m_input_manager->reset_released();

    while (SDL_PollEvent(&event))
    {
      handle_modifier_keys(event);

      switch (event.type)
      {
      case SDL_KEYDOWN:
      {
        const KeyboardKey key = convert(event);

        if (key == KeyboardKey::Escape) {
          close();
          break;
        }

        if (event.key.repeat) {
          m_input_manager->set_held(key);
        } else {
          m_input_manager->set_pressed(key);
        }

        break;
      }
      case SDL_KEYUP:
      {
        const KeyboardKey key = convert(event);

        if (key == KeyboardKey::Escape) {
          break;
        }

        m_input_manager->set_released(key);
        m_input_manager->reset_pressed(key);

        break;
      }
      case SDL_MOUSEMOTION:
      {

        break;
      }
      case SDL_MOUSEBUTTONDOWN:
      {

        break;
      }
      case SDL_MOUSEBUTTONUP:
      {

        break;
      }
      case SDL_QUIT:
      {
        close();
        break;
      }
      }
    }
  }

  void Window::swap_buffer()
  {
    SDL_GL_SwapWindow(m_handle);
  }


  U32 Window::get_width() const
  {
    return m_settings.width;
  }

  U32 Window::get_height() const
  {
    return m_settings.height;
  }

  void Window::register_input_manager(InputManager& input_manager)
  {
    m_input_manager = &input_manager;
  }

  void Window::destroy()
  {
    if (!is_null()) {
      TELLA_LOG_INTERNAL("Destroying window...");

      if (m_context) {
        SDL_GL_DeleteContext(m_context);
      }

      SDL_DestroyWindow(m_handle);
      SDL_Quit();
    }
  }

  void Window::handle_modifier_keys(const SDL_Event& event)
  {
    if (event.key.keysym.mod & KMOD_LCTRL) {
      m_input_manager->set_held(KeyboardKey::CtrlLeft);
    } else {
      m_input_manager->reset_pressed(KeyboardKey::CtrlLeft);
    }

    if (event.key.keysym.mod & KMOD_RCTRL) {
      m_input_manager->set_held(KeyboardKey::CtrlRight);
    } else {
      m_input_manager->reset_pressed(KeyboardKey::CtrlRight);
    }

    if (event.key.keysym.mod & KMOD_LALT) {
      m_input_manager->set_held(KeyboardKey::AltLeft);
    } else {
      m_input_manager->reset_pressed(KeyboardKey::AltLeft);
    }

    if (event.key.keysym.mod & KMOD_RALT) {
      m_input_manager->set_held(KeyboardKey::AltRight);
    } else {
      m_input_manager->reset_pressed(KeyboardKey::AltRight);
    }
  }

}  // tella
