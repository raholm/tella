#pragma once

#include <string>

#include <SDL2/SDL.h>

#include "types.h"
#include "input_manager.h"

namespace tella {

  class Window final
  {
  public:
    struct Settings
    {
    public:
      Settings();

    public:
      U32 width;
      U32 height;
      std::string title;

    };

  public:
    Window(const Settings& settings = Settings());

    ~Window();

    bool is_null() const;
    bool should_close() const;

    void poll_events();
    void swap_buffer();

    void register_input_manager(InputManager& input_manager);

    void close();

    U32 get_width() const;
    U32 get_height() const;


  private:
    Window(const Window&) = delete;
    Window(Window&&) = delete;

    Window& operator=(const Window&) = delete;
    Window& operator=(Window&&) = delete;

    void destroy();
    void handle_modifier_keys(const SDL_Event& event);

  private:
    Settings m_settings;
    bool m_should_close;

    SDL_Window* m_handle;
    SDL_GLContext m_context;

    InputManager* m_input_manager;

  };

}  // tella
